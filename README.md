# BookShelf

## Descriptrion
This project allows users to create account in BookShelf application using username and password. Any user can view the book information such as book name, author, publisher and published year. Once user logged in, user can add new books to the list, delete and update book information. So this project acts as a centralized library for books. Added custom ORM as a dependency to this project to interact with the database.

## Technologies Used
* Java 8
* JUnit
* Apache Maven
* Jackson library (for JSON marshalling/unmarshalling)
* PostGreSQL deployed on AWS RDS
* DBeaver
* Tomcat


## Features
* User can create account in BookShelf
* Any User(with/without account) can view the entire book information from the database
* Only logged in user can add new books to the list
* User needs to be signed in inorder to delete or update book information

## Custom ORM Repository
[Custom ORM](https://gitlab.com/210614-java-react-enterprise/project-1/team-6-orm)

## Contributors
[Marc Hartley](https://gitlab.com/har09028)

[Carlos Galvan](https://gitlab.com/CarlosGalvan)

[Rensy Aikara](https://gitlab.com/RensyAikara)
