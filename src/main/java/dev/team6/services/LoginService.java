package dev.team6.services;

import dev.team6.daos.LoginDao;
import dev.team6.models.Login;

public class LoginService {
    private LoginDao loginDao;

    public LoginService(LoginDao loginDao){
        this.loginDao = loginDao;
    }
    public String addNew(Login login) {
        String result = loginDao.addNewLogin(login);
        return result;
    }
}
