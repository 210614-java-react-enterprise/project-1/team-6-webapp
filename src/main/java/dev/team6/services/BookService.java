package dev.team6.services;

import dev.team6.daos.BookDao;
import dev.team6.models.Book;

import java.util.List;

public class BookService {

    private BookDao bookDao;

    public BookService(BookDao bookDao){
        this.bookDao = bookDao;
    }

    public List<? extends Book> getAll(){
       return bookDao.getAllBooks();
    }

    public boolean addNew(Book book){
        boolean result = bookDao.addNewBook(book);
        return result;
    }

    public boolean deleteAllBooks(Book book) { return bookDao.deleteAllBooks(book); }

    public boolean deleteBook(Book book, String column, String value) { return bookDao.deleteBook(book, column, value);}

    public List<Object> getByField(String keyParams, String valueParams){
        return bookDao.getByField(keyParams, valueParams);
    }
    public Object getOneByField(String keyParam, String valueParam) {
        return bookDao.getOneByField(keyParam, valueParam);
    };
    public boolean updateBook(Book book, String column, String value) { return bookDao.updateBook(book, column, value);}


}
