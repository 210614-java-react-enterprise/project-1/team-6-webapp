package dev.team6.daos;

import dev.team6.models.Book;

import java.util.List;

public interface BookDao {

    List<? extends Book> getAllBooks();
    boolean addNewBook(Book newBook);
    boolean deleteAllBooks(Book bookTable);
    boolean deleteBook(Book book, String column, String value);
    List<Object> getByField(String keyParams, String valueParams);
    Object getOneByField(String keyParam, String valueParam);
    boolean updateBook(Book book, String column, String value);

}
