package dev.team6.daos;

import dev.team6.models.Book;

import java.util.List;

public class BookDaoImpl implements BookDao{

TransactionDao transDao = new TransactionDaoImpl();
DataBaseDao dbDao = new DataBaseDaoImpl();
Book book = new Book();

    @Override
    public List<? extends Book> getAllBooks() {
        List<? extends Book> books = transDao.findAll(book);
        return books;
    }

    @Override
    public boolean addNewBook(Book newBook) {
        try {
            Book book = (Book) transDao.findOne(newBook, "name", newBook.getName());
            if (book == null) {
                transDao.createRecord(newBook);
                return true;
            }
        }catch(Exception e){
            transDao.createRecord(newBook);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteAllBooks(Book bookTable) {
        return dbDao.dropTable(bookTable);
    }

    @Override
    public boolean deleteBook(Book book, String column, String value) {
        Book newBook = (Book) transDao.findOne(book, column, value);
        if(newBook != null) {
            try {
                int intValue = Integer.parseInt(value);
                transDao.delete(book, column, intValue);
                return true;
            } catch (NumberFormatException e) {
                transDao.delete(book, column, value);
                return true;
            }
        }
        else{
            return false;
        }
    }

    @Override
    public List<Object> getByField(String keyParams, String valueParams) {
        return transDao.findAllByField(book, true, keyParams, valueParams);
    }

    @Override
    public boolean updateBook(Book book, String column, String value) {
        return transDao.update(book, column, value, book.getId());
    }

    @Override
    public Object getOneByField(String keyParam, String valueParam) {
        return transDao.findOne(book, keyParam, valueParam);
    }
}
