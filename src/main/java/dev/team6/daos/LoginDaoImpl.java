package dev.team6.daos;

import dev.team6.models.Login;

public class LoginDaoImpl implements LoginDao{
    TransactionDao transDao = new TransactionDaoImpl();
    Login login = new Login();

    @Override
    public String addNewLogin(Login newLogin){
        try{
            Login login = (Login) transDao.findOne(newLogin, "loginName", newLogin.getLoginName());
            if (login == null) {
                transDao.createRecord(newLogin);
                return "ADDED";
            }
            if (login != null) {
                if (login.getLoginPassword().equals(newLogin.getLoginPassword())) {
                    return "VERIFIED";
                } else {
                    return "DENIED";
                }
            }
        }catch(Exception e){
            transDao.createRecord(newLogin);
            return "ADDED";
        }
        return "ERROR";
    }
}
