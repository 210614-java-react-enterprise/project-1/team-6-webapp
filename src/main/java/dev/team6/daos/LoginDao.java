package dev.team6.daos;

import dev.team6.models.Login;

public interface LoginDao {
    public String addNewLogin(Login newLogin);
}
