package dev.team6.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team6.daos.LoginDaoImpl;
import dev.team6.models.Login;
import dev.team6.services.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {
    public LoginServlet(){};

    private LoginService loginService = new LoginService(new LoginDaoImpl());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(BufferedReader reader = req.getReader()) {
            PrintWriter pw = resp.getWriter();
            String loginJson = reader.readLine();
            ObjectMapper objMapper = new ObjectMapper();
            Login login = objMapper.readValue(loginJson, Login.class);
            String userName = login.getLoginName();
            String userPassword = login.getLoginPassword();

            if(userName != null && userPassword != null) {
                String result = null;
                result = loginService.addNew(login);

                if (result == "ADDED") {
                    resp.setStatus(201);
                    pw.write("Welcome " + userName +"..... Login credentials added successfully");
                    HttpSession session = req.getSession();
                    session.setAttribute("loggedIn", true);
                    session.setAttribute("username", userName);
                    //create session
                } else if (result == "VERIFIED") {
                    pw.write(loginJson + " Login credentials already exists in the database, Verified username and password, access granted");
                    //create session
                    HttpSession session = req.getSession();
                    session.setAttribute("loggedIn", true);
                    session.setAttribute("username", userName);
                } else if (result == "DENIED") {
                    //unauthorized
                    resp.sendError(401, "Invalid Credentials");
                } else if (result == "ERROR") {
                    //account cannot be created
                    resp.sendError( 400,"Unable to create account");
                }
            }
            else{
                resp.sendError(400, "UserName and Password cannot be null");
            }
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(403, "Not allowed to access login details");
    }
}
