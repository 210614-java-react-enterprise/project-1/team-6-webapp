package dev.team6.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team6.daos.BookDaoImpl;
import dev.team6.models.Book;
import dev.team6.services.BookService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

public class BookServlet extends HttpServlet {

    private BookService bookService= new BookService(new BookDaoImpl());

    public BookServlet(){};


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Book book = new Book();
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please authenticate before making request");
            return;
        }
        else {
            Enumeration paramNames = req.getParameterNames();
            String queryParamKey = (String)paramNames.nextElement();
            String queryParamValue = req.getParameter(queryParamKey);
            System.out.println(queryParamKey);
            //String queryParamValue = req.getParameter(queryParamKey);
            Book book = (Book)bookService.getOneByField(queryParamKey, queryParamValue);

            queryParamKey = (String)paramNames.nextElement();
            queryParamValue = req.getParameter(queryParamKey);
            System.out.println(queryParamKey);
            //queryParamValue = req.getParameter(queryParamKeyTwo);
            if (bookService.updateBook(book, queryParamKey, queryParamValue)) {
                System.out.println("Updated book");
            } else {
                System.out.println("Couldn't update book");
            }
        }
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Book book = new Book();
        String dropTable = req.getParameter("drop-table");
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please authenticate before making request");
            return;
        }
        else {
            try (PrintWriter pw = resp.getWriter();) {

                // Drops the book table if drop-table == "yes"
                if (dropTable != null) {
                    if (dropTable.equalsIgnoreCase("yes")) {
                        if (bookService.deleteAllBooks(book)) {
                            resp.setStatus(201);
                            pw.write("Successfully dropped all books");
                        } else {
                            resp.sendError(400, "Error dropping book table");
                        }

                    } else {
                        resp.sendError(406, "Only accepts \"yes\" as input");
                    }
                    return;
                }

                String queryParamKey = req.getParameterNames().nextElement();
                String queryParamValue = req.getParameter(queryParamKey);

                if (queryParamKey != null && queryParamValue != null) {
                    if (bookService.deleteBook(book, queryParamKey, queryParamValue)) {
                        pw.write(" Deleted book(s) where " + queryParamKey + " = " + queryParamValue);
                    } else {
                        pw.write(" Could not delete record where " + queryParamKey + " = " + queryParamValue + " Record not found in the database");
                    }
                }
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please authenticate before making request");
            return;
        }
        else {
            try(BufferedReader reader = req.getReader()) {
                PrintWriter pw = resp.getWriter();
                String bookJson = reader.readLine();
                ObjectMapper objMapper = new ObjectMapper();
                Book book = objMapper.readValue(bookJson, Book.class);

                if (book != null && book.getName().length() > 100) {
                    resp.sendError(400, "Name is too long");
                    return;
                }
                boolean result = bookService.addNew(book);
                if (result) {
                    resp.setStatus(201);
                    pw.write(bookJson + " Record added successfully");
                } else {
                    pw.write(bookJson + " Record already exists in the database");
                }
            }
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String isQuery = req.getQueryString();
        String jsonBooks = "";
        // if query parameter doesn't exists, get all records from db
        if(isQuery == null){
            List<? extends Book> books = bookService.getAll();
            ObjectMapper objectMapper = new ObjectMapper();
            jsonBooks = objectMapper.writeValueAsString(books);
            if(books == null){
                resp.sendError(404, "Requested Data is not found");
            }
        }
        // if query parameter exists, get all records with given parameter
        else {
            String queryParamKey = req.getParameterNames().nextElement();
            String queryParamValue = req.getParameter(queryParamKey);
            List<Object> booksNew = bookService.getByField(queryParamKey,queryParamValue);
            ObjectMapper objectMapper = new ObjectMapper();
            jsonBooks = objectMapper.writeValueAsString(booksNew);
            if(booksNew.isEmpty()){
                resp.sendError(404, "Requested Data is not found");
            }
        }
        try (PrintWriter pw = resp.getWriter()) {
            pw.write(jsonBooks);
        }
    }
}
